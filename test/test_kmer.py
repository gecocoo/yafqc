import os
import sys
import numpy as np
import yafqc.kmer as kmer
import yafqc.dna_encode as enc
import pytest
from collections import Counter
from numba import njit, config
from importlib import reload


@pytest.fixture(scope="function", autouse=True)
def disable_jit(monkeypatch):
    monkeypatch.setenv("NUMBA_DISABLE_JIT", "1")
    config.reload_config()
    reload(kmer)
    reload(enc)


def test_dnadecode_act():
    assert kmer.dna_decode(0b000111, k=3) == "ACT"


def test_dnadecode_gattaca():
    assert kmer.dna_decode(0b10001111000100, 7) == "GATTACA"


def test_dnadecode_gattaca_leading_as():
    assert kmer.dna_decode(0b10001111000100, 7 + 3) == "AAA" + "GATTACA"


def test_dna_decode_error():
    with pytest.raises(ValueError):
        kmer.dna_decode(123, 3)


def test_create_byte_rev_table():
    table = kmer.create_byte_rev_table()
    assert table.size == 256


def test_comp():
    c1 = enc.dna_encode("GATTACA")
    c2 = enc.dna_encode("CTAATGT")
    r = kmer.comp(c1, 7)
    assert c2 == r


def test_comp2():
    c1 = enc.dna_encode("GATTACAG")
    c2 = enc.dna_encode("CTAATGTC")
    r = kmer.comp(c1, 8)
    assert c2 == r


def test_rev_compl():
    s1 = enc.dna_encode("GATAC")
    s2 = enc.dna_encode("GTATC")
    r = kmer.rev_compl(s1, 5)
    assert s2 == r


def test_make_kmer_processor_for_ACGTATTACGAG_k4():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b00_01_10_11,  # ACGT
        0b01_10_11_00,  # CGTA
        0b10_11_00_11,  # GTAT
        0b11_00_11_11,  # TATT
        0b00_11_11_00,  # ATTA
        0b11_11_00_01,  # TTAC
        0b11_00_01_10,  # TACG
        0b00_01_10_00,  # ACGA
        0b01_10_00_10,  # CGAG
    ]

    def index_iterator():
        index = 0
        while True:
            yield index
            index += 1

    def check_arguments_for_f(kmer, it):
        idx = next(it)
        assert kmer == correct_kmers[idx]

    # Creates a sequence and encodes it:
    seq = "ACGTATTACGAG"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = kmer.make_kmer_processor(check_arguments_for_f, 4, use_canonical=False)

    # Calls the processor:
    processor(arr, index_iterator())


def test_make_kmer_processor_for_ACGTATTACGAG_k4_canonical():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b00_01_10_11,  # ACGT
        0b01_10_11_00,  # CGTA
        0b00_11_00_01,  # ATAC(reverse complement of GTAT)
        0b00_00_11_00,  # AATA(reverse complement of TATT)
        0b00_11_11_00,  # ATTA
        0b10_11_00_00,  # GTAA(reverse complement of TTAC)
        0b01_10_11_00,  # CGTA(reverse complement of TACG)
        0b00_01_10_00,  # ACGA
        0b01_10_00_10,  # CGAG
    ]

    def index_iterator():
        index = 0
        while True:
            yield index
            index += 1

    def check_arguments_for_f(kmer, it):
        idx = next(it)
        assert kmer == correct_kmers[idx]

    # Creates a sequence and encodes it:
    seq = "ACGTATTACGAG"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = kmer.make_kmer_processor(check_arguments_for_f, 4, use_canonical=True)

    # Calls the processor:
    processor(arr, index_iterator())


def test_make_kmer_processor_for_TTAGCA_k2():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b11_11,  # TT
        0b11_00,  # TA
        0b00_10,  # AG
        0b10_01,  # GC
        0b01_00,  # CA
    ]

    # def check_arguments_for_f(kmer, args):
    #     assert kmer == correct_kmers[args]
    #     args += 1

    #     return

    def index_iterator():
        index = 0
        while True:
            yield index
            index += 1

    def check_arguments_for_f(kmer, it):
        idx = next(it)
        assert kmer == correct_kmers[idx]

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = kmer.make_kmer_processor(check_arguments_for_f, 2, use_canonical=False)

    # Calls the processor:
    processor(arr, index_iterator())


def test_make_kmer_processor_for_TTAGCA_k2_canonical():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b00_00,  # AA is canonical of TT
        0b11_00,  # TA is rev comp to itself, thus canonical
        0b00_10,  # AG < CT (01_11)
        0b10_01,  # GC rev comp to itself
        0b01_00,  # CA < TG (11_01)
    ]

    def index_iterator():
        index = 0
        while True:
            yield index
            index += 1

    def check_arguments_for_f(kmer, it):
        idx = next(it)
        assert kmer == correct_kmers[idx]

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = kmer.make_kmer_processor(check_arguments_for_f, 2, use_canonical=True)

    # Calls the processor:
    processor(arr, index_iterator())


def test_make_kmer_processor_error_on_k_smaller_1():
    # Tests whether the make_kmer_processor function raises an error on k being smaller 1:

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    np.frombuffer(seq.encode(), dtype=np.uint8)

    with pytest.raises(ValueError):
        kmer.make_kmer_processor(None, -1, use_canonical=False)


def test_make_kmer_processor_k_longer_than_arr():
    # Tests whether the make_kmer_processor function raises an error on the array being shorter then the length of a kmer:

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)
    k = len(seq) + 1

    processor = kmer.make_kmer_processor(None, k, False)

    # Calls the processor:
    processor(arr, 0)


def test_process_kmers():
    rev_compl = False
    k = 3
    counter = np.zeros((4 ** k), dtype=np.uint32)

    @njit
    @kmer.process_kmers(k=k, use_canonical=rev_compl)
    @njit
    def count_kmers(kmer: int, counter: np.ndarray):
        counter[kmer] += 1

    sequence = "GANTTACAGNNATTACANN"
    count_kmers(np.frombuffer(sequence.encode(), dtype=np.uint8), counter)

    correct = np.zeros((4 ** k), dtype=np.uint32)
    kmers = ["TTA", "TAC", "ACA", "CAG", "ATT", "TTA", "TAC", "ACA"]
    for m in kmers:
        correct[enc.dna_encode(m)] += 1

    assert (counter == correct).all()


def test_process_kmers_n_only():
    rev_compl = False
    k = 3
    counter = np.zeros((4 ** k), dtype=np.uint32)

    @njit
    @kmer.process_kmers(k=k, use_canonical=rev_compl)
    @njit
    def count_kmers(kmer: int, counter: np.ndarray):
        counter[kmer] += 1

    sequence = "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"
    count_kmers(np.frombuffer(sequence.encode(), dtype=np.uint8), counter)

    correct = np.zeros((4 ** k), dtype=np.uint32)

    assert (counter == correct).all()
