import os
import subprocess
import itertools
import pytest


def test_report(tmp_path):
    assert (
        os.system(
            f"yafqc report < test/resources/results.json > {tmp_path}/report.html"
        )
        == 0
    )
    # Compare files
    with open("test/expected/report.html", "r") as expected, open(
        f"{tmp_path}/report.html", "r"
    ) as test_file:
        for expected_line, line in itertools.zip_longest(expected, test_file):
            # Ignore line that might differ due to timestamp or version
            # Ignore line with gcSpecs due to randomly generated normal distribution
            if not (
                "version" in line or "created" in line or "normal_distribution" in line
            ):
                assert line == expected_line


def test_contamination_report(tmp_path):
    assert (
        os.system(
            f"yafqc report < test/resources/contamination_results.json > {tmp_path}/contamination_report.html"
        )
        == 0
    )
    # Compare files
    with open("test/expected/contamination_report.html", "r") as expected, open(
        f"{tmp_path}/contamination_report.html", "r"
    ) as test_file:
        for expected_line, line in itertools.zip_longest(expected, test_file):
            # Ignore line that might differ due to timestamp or version
            # Ignore line with gcSpecs due to randomly generated normal distribution
            if not (
                "version" in line or "created" in line or "normal_distribution" in line
            ):
                assert line == expected_line
