import os
import pytest
from numba import config
from importlib import reload
import subprocess

"""
Tests for the 'Yet Another FASTQ Quality Control' tool
"""


@pytest.fixture(scope="function", autouse=False)
def disable_jit(monkeypatch):
    monkeypatch.setenv("NUMBA_DISABLE_JIT", "1")
    config.reload_config()


def test_yafqc_process_nohash(tmp_path, disable_jit):
    assert (
        os.system(
            f"yafqc process test/resources/shortExample.fastq --nohash > {tmp_path}/test_result.json"
        )
        == 0
    )


def test_yafqc_process(tmp_path):
    assert (
        os.system(
            f"yafqc process test/resources/shortExample.fastq > {tmp_path}/test_result.json"
        )
        == 0
    )


def test_yafqc_process_broken_file(tmp_path):
    assert (
        os.system(
            f"yafqc process test/resources/brokenExample.fastq > {tmp_path}/test_result.json"
        )
        > 0
    )


def test_yafqc_process_options(tmp_path, disable_jit):
    assert (
        os.system(
            f"yafqc process test/resources/shortExample.fastq --nohash -c -t 8 -k 27 > {tmp_path}/test_result.json"
        )
        == 0
    )


def test_process_into_report(tmp_path):
    input = "test/resources/shortExample.fastq"
    assert (
        os.system(
            f"yafqc process {input} --nohash | yafqc report > {tmp_path}/test_report.html"
        )
        == 0
    )


def test_contamination(tmp_path):
    input_fasta = "test/resources/short.fasta"
    input_fastq = "test/resources/shortExample.fastq"
    assert (
        os.system(f"yafqc contamination {input_fasta} -o {tmp_path}/reftable.gecocoo")
        == 0
    )
    assert (
        os.system(
            f"yafqc process {input_fastq} -r {tmp_path}/reftable.gecocoo > {tmp_path}/test_result.json"
        )
        == 0
    )


def test_contamination_fails_with_small_table(tmp_path):
    input_fasta = "test/resources/short.fasta"
    result = subprocess.run(
        f"yafqc contamination {input_fasta} -o {tmp_path}/reftable.gecocoo -s 1",
        shell=True,
    )
    assert result.returncode == 1
