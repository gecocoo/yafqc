import os
import sys
import numpy as np
import pytest
import yafqc.dna_encode as enc
from numba import config
from importlib import reload


@pytest.fixture(scope="function", autouse=True)
def disable_jit(monkeypatch):
    monkeypatch.setenv("NUMBA_DISABLE_JIT", "1")
    config.reload_config()
    reload(enc)


def test_dnaencode_act():
    seq = "ACT"
    seq_int = 0b000111
    assert seq_int == enc.dna_encode(seq)


def test_dnaencode_str_gattaca():
    assert enc.dna_encode("GATTACA") == 0b10001111000100


# def test_dna_encode_str_array():
#     assert _dna_encode_str_array("GATTACA") == 0b10001111000100


# def test_dna_encode_str_2_at_a_time_array():
#     assert _dna_encode_str_array_2_at_a_time("GATTACA") == 0b10001111000100


def test_dna_encode_str_if():
    assert enc._dna_encode_str_if("GATTACA") == 0b10001111000100


def test_dnaencode_bytes_gattaca():
    assert enc.dna_encode(b"GATTACA") == 0b10001111000100


def test_dnaencode_nparray_gattaca():
    assert (
        enc.dna_encode(np.frombuffer("GATTACA".encode(), dtype=np.uint8))
        == 0b10001111000100
    )


def test_dnaencode_bytes_array_gattaca():
    assert enc._dna_encode_bytes_array(bytes("GATTACA", "utf-8")) == 0b10001111000100


# def test_dnaencode_bytes_array_2_at_a_time_gattaca():
#     assert _dna_encode_bytes_array_2_at_a_time(
#         bytes("GATTACA", 'utf-8')) == 0b10001111000100


# def test_dnaencode_bytes_if_gattaca():
#     assert _dna_encode_bytes_if(bytes("GATTACA", 'utf-8')) == 0b10001111000100


# def test_dnaencode_ndarray_array_gattaca():
#     assert _dna_encode_ndarray_array(
#         np.frombuffer("GATTACA".encode(), dtype=np.uint8)) == 0b10001111000100


def test_dnaencode_ndarray_array_2_at_a_time_gattaca():
    assert (
        enc._dna_encode_ndarray_array_2_at_a_time(
            np.frombuffer("GATTACA".encode(), dtype=np.uint8)
        )
        == 0b10001111000100
    )


# def test_dnaencode_ndarray_if_gattaca():
#     assert _dna_encode_ndarray_if(
#         np.frombuffer("GATTACA".encode(), dtype=np.uint8)) == 0b10001111000100


def test_dna_encode_wrong_type():
    with pytest.raises(NotImplementedError):
        enc.dna_encode(["A", "G", "T"])
