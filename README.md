# Yet Another FastQ Quality Control

## Cloning the Repository

Cloning the repository via SSH:
```
git clone --recurse-submodules git@gitlab.com:gecocoo/yafqc.git
```

Or via HTTPS:
```
git clone --recurse-submodules https://gitlab.com/gecocoo/yafqc.git
```

Cloning/Updating geocoo submodule, when repository is already cloned:
```
git submodule update --remote
```

## Installation
**Prerequisites:**

Install Anaconda or miniconda.  

Setup conda environment:  
```
conda env create -f environment.yml
```
Or update your existing environment:  
```
conda env update -f environment.yml
```

When developing make sure to install the git hook scripts
```
pre-commit install
``` 

**Install yafqc:**  

Activate the conda environment:   
```
conda activate yafqc
```   

Install gecocoo submodule:
```
pip install -e gecocoo
```
Install yafqc:
```
python setup.py develop
```
or  
```
pip install -e .
```

## Updating your Installation   

To check out and install a new version of gecocoo:
```
git submodule update --remote
pip install -e gecocoo
```
Reinstall `yafqc` after making changes:
```
pip install -e .
```



## Subcommands

### contamination
Processes given fasta files to build a gecocoo hashtable of contained kmers.   
Outputs a serialized gecocoo hashtable.   
For large genomes, we recommend estimating the kmer count (e.g. via `ntcard`) and using it via `-s kmer_count`.   

Example:
```
yafqc contamination first_genome.fasta another_genome.fasta -o reftable.gecocoo   
```

or by using a config file:

```
yafqc contamination  first_genome.fasta another_genome.fasta --config config.yaml
```


### process
Processes a given fastq file by calculating several statistics.
Results are written to `stdout` as JSON.  

Example:  
```
yafqc process my_file.fastq > results.json
``` 

or with contamination check:

```
yafqc process my_file.fastq -r reftable.gecocoo > results.json
```

### report
Create a HTML report with plots for a given JSON result file.

To create a report from a previously created `results.json`:

```
yafqc report < results.json > report.html
```

or directly use the output given from `yafqc process`:

```
yafqc process my_file.fastq | yafqc report > report.html
```

Check out an example of a generated report [here](https://gecocoo.gitlab.io/yafqc/doc_report.html).

## Related Tools

- [FastQC](https://gitlab.com/gecocoo/orga/-/wikis/FastQC)
- [FastQ-Screen](https://gitlab.com/gecocoo/orga/-/wikis/FastQ-Screen-%C3%9Cberblick)
- [fastq-scan](https://gitlab.com/gecocoo/orga/-/wikis/fastq-scan)

## Goals

[PG Ziele](https://md.fachschaften.org/-nOtfYe1RgKWlYiYcVh8qw?view#)

## Documentation
Have a look at the [documentation](https://gecocoo.gitlab.io/yafqc/).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
