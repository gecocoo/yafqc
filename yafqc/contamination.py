""" Contamination check
Offers functionalty for generating a hashtbale of k-mers of reference genomes. 
This is possible for a single or multiple FASTA files, it offers multithreading and chunkwise reading and
processing of FASTA files.

This file requires 'numpy' and 'numba' to be installed on the device it is going
to be used on.

This file can be imported by another .py file and contains the following
functions:

    * create_reference_tables - Generates a process function for reading FASTA files and generating a adjustable reference table for contamination checks by using GECOCOO. 
    * count_kmers - Counts the occurrence of each k-mer in each reference genome.
    * creator - Changes the storage of the configurated hashtable inplace.
"""


import numpy as np
from numba import njit
import datetime

import os
import multiprocessing
import sys
from pathlib import Path
from strictyaml.exceptions import YAMLValidationError

from gecocoo.generator import generate_hashtable
from gecocoo.valuemodules import store, counting
from gecocoo.config import (
    Config,
    HashFunction,
    ValueModuleConfig,
    StorageConfig,
    StorageMode,
)
from gecocoo.config.file import load_from_yaml
from gecocoo.helper import get_qualified_name
from gecocoo.hashing import xorlinear, swaplinear
from gecocoo.config.lowlevel import ConfigError

from concurrent.futures import ThreadPoolExecutor, as_completed, wait, FIRST_COMPLETED

from yafqc.kmer import process_kmers
from yafqc.kmer import number_canonicals
from yafqc.dnaio import fasta_reads
from yafqc.utils import log, mebi_bytes

from yafqc.flags import flags_module, generate_flags
from numba.core.errors import NumbaExperimentalFeatureWarning

VALUE_MODULE = get_qualified_name(flags_module)


def create_reference_tables(
    no_files, input_size, output_path, threads, cli_k, canonical, config, tablesize=None
):
    """Generates a process function for reading FASTA files and generating a reference table for contamination checks.


    Args:
        no_files int:
            Number of reference genomes
        input_size int:
            Size of the inserted fasta file for approximating necessary hashtable size.
        output_path str:
            Output path for the built hashtable
        threads int:
            Number of threads
        k int:
            Length of each k-mer
        canonical Bool:
            If canonical k-mers should be used.
        config str:
            Path to config file for reference hashtable

    Returns:
        Generated function
    """

    if config:
        with open(config) as config_file:
            try:
                hash_conf = load_from_yaml(config_file)
                if threads > 1 and (
                    hash_conf.storage is None or not hash_conf.storage.concurrent
                ):
                    log(
                        "Warning: Number of threads is > 1 but concurrent mode is set to False. Processing will take longer."
                    )
                if hash_conf.k != cli_k:
                    log(
                        f"""Warning: k given in config file ({hash_conf.k}) differs from k given in command line: {cli_k}
                    Prioritizing config file."""
                    )

                if hash_conf.value_module.module_name != VALUE_MODULE:
                    log(
                        f"Error: Value module is '{hash_conf.value_module.module_name}', but should be '{VALUE_MODULE}'"
                    )
                    sys.exit(1)

                if (
                    len(hash_conf.value_module.parameters) != 1
                    or hash_conf.value_module.parameters[0] < no_files
                ):
                    log("Error: invalid value module parameters")
                    sys.exit(1)

            except YAMLValidationError as err:
                log("Error: failed loading config file:", config)
                log(err)
                sys.exit(1)
    else:
        mode = False
        if threads > 1:
            mode = True
        hash_conf = Config(
            load_factor=0.90,
            bucket_size=4,
            k=cli_k,
            value_module=ValueModuleConfig(VALUE_MODULE, (no_files,)),
            hash_functions=[
                HashFunction(get_qualified_name(xorlinear), []),
                HashFunction(get_qualified_name(xorlinear), []),
            ],
            storage=StorageConfig(
                mode=StorageMode.BUCKETS,
                concurrent=mode,
                disable_quotienting=False,
            ),
        )

    # estimate number of k-mers in the file
    # input_size : number of bytes of input file
    if hash_conf.n:
        log("using table size given in configuration...")
        tablesize = hash_conf.n
    elif tablesize is None:
        log("estimating table size based on inputs...")
        seq_size = int(input_size)
        expected_kmers = max(0, seq_size - hash_conf.k + 1)
        if canonical:
            comb_kmers = number_canonicals(hash_conf.k)
        else:
            comb_kmers = 4 ** hash_conf.k

        tablesize = min(expected_kmers, comb_kmers)

    log("required table size:", tablesize)
    try:
        hashtable, utils = generate_hashtable(hash_conf, tablesize)
    except ConfigError as err:
        log("Error: Unable to generate hashtable:", err)
        sys.exit(1)

    log(f"actual table size: {utils.capacity}")
    log(f"table size in memory (MiB): {mebi_bytes(utils.byte_size)}")
    insert = hashtable.insert_single

    ref_flags = generate_flags(no_files)
    ref_flags = np.array(ref_flags)

    @njit
    @process_kmers(k=hash_conf.k, use_canonical=canonical)
    @njit(nogil=True)
    def count_kmers(kmer: int, storage_flag):
        """Function to be called for each k-mer in a sequence to count the occurrence of each k-mer by the use of
        a decorator.

        Parameters
        ----------
        k : int
           Length of each k-mer.
        use_canonical : Bool
            Flag that indicates whether the reverse complement of each k-mer shall be also counted.
        kmer: int
            Encoded representation of the seen k-mer, which shall be counted.
        storage_flags : Tuple
            Tuple of a hashtable.storage for saving k-mers and reference_flags for indicating from which refrence genome.

        Returns
        -------
        """
        storage = storage_flag[0]
        flag = storage_flag[1]
        insert(storage, kmer, flag)

    @njit(nogil=True)
    def creator(buf, storage, flag):
        """Function for processing of each chunk of the reference sequence. Changes the storage of the configurated hashtable inplace.

        Parameters
        ----------
        buf : numpy array
            Contains all characters of the file as byte.
        storage:
            Hashtable storage for saving k-mers
        flags: Bitflags for indicating which reference genome is processed

        Returns
        -------
        """
        count_kmers(buf, args=(storage, flag))
        return

    def create_table_from_fasta(fasta):
        """Function for reading fasta file

        Parameters
        ----------
        fasta str:
            Input file(s)

        Returns
        -------
        Generated hashtables with all k-kmers of the reference genomes
        """

        def progress_report(processed: int):
            return f"processed {mebi_bytes(processed)}/{mebi_bytes(input_size)} MiB => {processed / input_size * 100:.01f}%"

        with ThreadPoolExecutor(max_workers=threads) as executor:
            log("starting building table")
            limit = 100
            futures = set()
            processed = 0
            last_process_report = 0
            for j, f in enumerate(fasta):
                try:
                    for chunk in fasta_reads(f):
                        if len(futures) >= limit:
                            done_futures, futures = wait(
                                futures, return_when=FIRST_COMPLETED
                            )
                            for d in done_futures:
                                d.result()

                        futures.add(
                            executor.submit(
                                creator, chunk[1], hashtable.storage, ref_flags[j]
                            )
                        )
                        processed += len(chunk[0]) + len(chunk[1])

                        # report progress at maximum every 0.5% steps
                        if (processed - last_process_report) / input_size >= 0.005:
                            executor.submit(
                                log,
                                progress_report(processed),
                                end="\r",
                                file=sys.stderr,
                            )
                            last_process_report = processed

                    for f in futures:
                        f.result()
                except Exception as err:
                    # log empty line to not write into a previous progress ouput
                    log(file=sys.stderr)
                    log(f"Failed to build table, GeCoCoo error: {err}")
                    sys.exit(1)

        # report 100% processed at the end
        log(progress_report(input_size))

        log("finished building table")
        log(f"load factor: {utils.current_load_factor:.03f}")
        log("unique k-mers:", utils.current_key_count)

        utils.metadata = {"ref_number": len(fasta), "files": fasta}

        dir_path = os.path.dirname(output_path)
        Path(dir_path).mkdir(parents=True, exist_ok=True)
        with open(output_path, "wb") as output_file:
            utils.serialize(output_file)

    return create_table_from_fasta


def main(args):
    now = datetime.datetime.now()
    log(f"#{now:%Y-%m-%d %H:%M:%S}: Begin processing")
    if args.fasta:
        if args.threadcount == 0:
            threads = multiprocessing.cpu_count()
        else:
            threads = args.threadcount
        input_size = 0
        for f in args.fasta:
            if not os.path.isfile(f):
                log("FASTA file not found:", f)
                sys.exit(1)
            input_size += os.stat(f).st_size

        # if not os.path.isdir(args.output):
        #     print("output directory invalid:", args.output)
        #     return

        process_read_from_fastq = create_reference_tables(
            len(args.fasta),
            input_size,
            args.output,
            threads,
            args.kmer,
            args.canonical,
            args.config,
            args.size,
        )
        process_read_from_fastq(args.fasta)
        now = datetime.datetime.now()
        log(f"#{now:%Y-%m-%d %H:%M:%S}: All Reads processed")
    else:
        assert False
