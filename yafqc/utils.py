import sys


def mebi_bytes(bytes: int) -> str:
    """Return the number of bytes formatted as mebibytes"""
    return f"{bytes / 1024**2:.02f}"


def log(*values, file=sys.stderr, **kwargs):
    """Log output to stderr by default and flush immediatly."""
    print(*values, file=file, flush=True, **kwargs)
