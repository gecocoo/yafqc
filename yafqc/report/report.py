""" FASTQ Processing File
Offers functionality for generating a html report from a json input.

This file requires 'numpy' to be installed on the device it is going
to be used on.

This file can be imported by another .py file and contains the following
functions:

    * create_report - Creates the report by loading a template, processing given data and filling plots with previously processed data.
    * embed_source - Help function to fetch scripts via http and include them locally in the html file with a script tag.
    * process_counter_data - Processes occurrences of k-mers to be embedded into Vega-Lite-plot.
    * process_sequence_quality_scores - Processes occurrences of average phred scores per read to be embedded into Vega-Lite-plot.
    * process_base_per_pos - Processes occurrences of bases as percentages for each position in a read to be embedded into Vega-Lite-plot.
    * process_read_length - Processes occurring read lengths to be embedded into Vega-Lite-plot.
    * process_distance_data - Processes distances to be embedded into Vega-Lite-plot.
    * process_gc_content - Processes occurrences of gc-content as percentages to be embedded into Vega-Lite-plot.
    * process_quality_per_pos_data - Processes distribution of phred scores per position in a read to be embedded into Vega-Lite-plot.

"""

import itertools
import json
import sys
import numpy as np
import jinja2
import requests
from pathlib import Path
from datetime import datetime
import os

from yafqc._version import VERSION
from yafqc.kmer import dna_decode


def create_report():
    """Creates the report by loading a template, processing given data and filling plots with previously processed data.

    Parameters
    ----------
    None

    Returns
    -------
    None
    """
    template_loader = jinja2.PackageLoader("yafqc", "report")
    template_env = jinja2.Environment(loader=template_loader)
    template_env.filters["embed_source"] = embed_source
    TEMPLATE_FILE = "report_template.html"
    template = template_env.get_template(TEMPLATE_FILE)

    base_path = Path(__file__).parent
    results = json.loads(sys.stdin.read())

    contamination = (
        "distances" in results and "base_distances" in results and "names" in results
    )

    # Process counter data
    counter_data = process_counter_data(results["counter"], results["k"])
    counter_path = base_path / "counter_specs.json"
    with open(counter_path, "r") as file:
        counter_specs = json.loads(file.read())
    counter_specs["data"]["values"] = counter_data
    counter_specs = json.dumps(counter_specs)

    # Process quality per pos data
    qpp_data = process_quality_per_pos_data(results["quality_per_pos"])
    qpp_path = base_path / "quality_per_pos_specs.json"
    with open(qpp_path, "r") as file:
        qpp_specs = json.loads(file.read())
    qpp_specs["data"]["values"] = qpp_data
    qpp_specs = json.dumps(qpp_specs)

    # Process seqence quality score
    sqc_data = results["mean_read_qualities"]
    sqc_path = base_path / "sequence_quality_score_specs.json"
    with open(sqc_path, "r") as file:
        sqc_specs = json.loads(file.read())
    sqc_specs["data"]["values"] = sqc_data
    sqc_specs = json.dumps(sqc_specs)

    # Process base per pos data
    bpp_data = process_base_per_pos(results["character_per_pos"])
    bpp_path = base_path / "base_per_pos_specs.json"
    with open(bpp_path, "r") as file:
        bpp_specs = json.loads(file.read())
    bpp_specs["data"]["values"] = bpp_data
    bpp_specs = json.dumps(bpp_specs)

    # Process read length data
    rle_data = process_read_length(results["read_lengths"])
    rle_path = base_path / "read_lengths_specs.json"
    with open(rle_path, "r") as file:
        rle_specs = json.loads(file.read())
    rle_specs["data"]["values"] = rle_data
    rle_specs = json.dumps(rle_specs)

    # Process gc content data
    gcc_data = results["gc_content"]
    gcc_path = base_path / "gc_content_specs.json"
    with open(gcc_path, "r") as file:
        gcc_specs = json.loads(file.read())
    gcc_specs["data"]["values"] = gcc_data
    gcc_specs = json.dumps(gcc_specs)

    readCount = count_reads(results["read_lengths"])

    if contamination:
        # Process distances data
        d_data = results["distances"]
        d_path = base_path / "distances_specs.json"
        with open(d_path, "r") as file:
            d_specs = json.loads(file.read())
        d_specs["data"]["values"] = d_data
        d_specs["title"] = (
            "Similarity to reference over " + str(readCount) + " sequences"
        )
        d_specs = json.dumps(d_specs)

        # Process base distance data
        bd_data = results["base_distances"]
        bd_path = base_path / "base_distances_specs.json"
        with open(bd_path, "r") as file:
            bd_specs = json.loads(file.read())
        bd_specs["data"]["values"] = bd_data
        bd_specs["title"] = (
            "Base similarity to reference over " + str(readCount) + " sequences"
        )
        bd_specs = json.dumps(bd_specs)

    _, file_name = os.path.split(results["file_name"])

    meta = {
        "file_name": {"name": "file name", "value": file_name},
        "threads": {"name": "threads", "value": results["threads"]},
        "canonical": {"name": "canonical", "value": results["canonical"]},
        "k": {"name": "k", "value": results["k"]},
        "processed reads": {"name": "processed reads", "value": readCount},
    }

    plots = {
        "k-mer quantities": {"short": "count", "specs": counter_specs},
        "base sequence quality": {"short": "base", "specs": qpp_specs},
        "sequence quality score": {"short": "qual", "specs": sqc_specs},
        "base sequence content": {"short": "cont", "specs": bpp_specs},
        "gc content": {"short": "gc", "specs": gcc_specs},
        "read lengths": {"short": "rlen", "specs": rle_specs},
    }

    if contamination:
        plots.update({"similarity": {"short": "dist", "specs": d_specs}})
        plots.update({"base similarity": {"short": "bdist", "specs": bd_specs}})
        meta.update({"ref_k": {"name": "k for reference", "value": results["ref_k"]}})

    output = template.render(
        version=VERSION,
        time=datetime.now().strftime("%Y-%m-%d %H:%M"),
        plots=plots,
        meta=meta,
    )
    sys.stdout.reconfigure(encoding="utf-8")
    sys.stdout.write(output)


def embed_source(src_url):
    """Help function to fetch scripts via http and include them locally in the html file with a script tag.

    Parameters
    ----------
    src_url : string
        Url of the source script

    Returns
    -------
    r.text : string
        Source code returned by the http request
    """
    r = requests.get(src_url)
    return r.text


def count_reads(data):
    c = 0
    for (_, count) in data.items():
        c += count
    return c


def process_counter_data(data, k):
    """Processes occurrences of k-mers to be embedded into Vega-Lite-plot.

    Parameters
    ----------
    data : dict
        Contains the occurrences of each possible k-mer as key-value-pair, therefore 4**k pairs
        The k-mer is represented as encoded index, see dna_encode.py
    k : int
        k-mere size used for processing.

    Returns
    -------
    results : list of dicts
        Contains the input for the Vega-Lite-plot to be embedded as 'values'
    """
    result = []
    values = []
    for _, c in data.items():
        values.append(c)
    values = np.array(values)
    std = np.std(values)
    avg = np.mean(values)

    for i, c in data.items():
        if c < (avg - std) or c > (avg + std):
            k_mer = dna_decode(int(i), int(k))
            entry = {"k_mer": k_mer, "count": c}
            result.append(entry)

    return result


def process_sequence_quality_scores(data):
    """Processes occurrences of average phred scores per read to be embedded into Vega-Lite-plot.

    Parameters
    ----------
    data : list of dicts
        Contains an entry for each phred score with the occurrence of each score over all processed reads

    Returns
    -------
    results : list of dicts
        Contains the input for the Vega-Lite-plot to be embedded as 'values'
    """
    result = []
    data = np.around(data).astype(int)
    data = np.bincount(data)
    for (i, occurrences) in enumerate(data):
        result.append({"count": int(occurrences), "score": i})
    return result


def process_base_per_pos(data):
    """Processes occurrences of bases as percentages for each position in a read to be embedded into Vega-Lite-plot.

    Parameters
    ----------
    data : array of arrays of type np.uint32
        The outer array represents each position in a read
        The inner array contains the occurrences of a, c, g, t and n (in this order) at index 0 to 4, as known from dna_encode.py

    Returns
    -------
    results : list of dicts
        Contains the input for the Vega-Lite-plot to be embedded as 'values'
    """
    results = []
    bases = ["A", "C", "G", "T", "N"]
    for (pos, occs) in enumerate(data):
        sum = 0
        for o in occs:
            sum += o
        for (base, count) in itertools.zip_longest(bases, occs):
            if sum != 0:
                results.append({"pos": pos, "base": base, "count": count / sum})
            else:
                results.append({"pos": pos, "base": base, "count": 0})
    return results


def process_read_length(data):
    """Processes occurring read lengths to be embedded into Vega-Lite-plot.

    Parameters
    ----------
    data : dict
        Dict containing each occurring read length with the length as key and the occurrences as value

    Returns
    -------
    results : list of dicts
        Contains the input for the Vega-Lite-plot to be embedded as 'values'
    """
    results = []
    for (length, count) in data.items():
        results.append(
            {
                "length": length,
                "count": count,
            }
        )
    return results


def process_distance_data(data, names):
    """Processes distances to be embedded into Vega-Lite-plot.

    Parameters
    ----------
    data : list of lists
        List containing each a list for every read that contains a list with the distance for every reference.
    names : list
        List containing the names of the references.

    Returns
    -------
    results : list of dicts
        Contains the input for the Vega-Lite-plot to be embedded as 'values'
    """
    results = []
    references = {}

    for name in names:
        references[name] = []
    for arr in data:
        for (i, val) in enumerate(arr):
            references[names[i]].append(round(val * 100))
    for key, arr in references.items():
        references[key] = np.bincount(arr)
        for _ in range(len(references[key]), 100):
            references[key] = np.append(references[key], [0])
        for (i, r) in enumerate(references[key]):
            res = {"name": key, "count": int(r), "percentage": i}
            results.append(res)
    return results


def process_gc_content(data):
    """Processes occurrences of gc-content as percentages to be embedded into Vega-Lite-plot.

    Parameters
    ----------
    data : array of type np.double
        Array containing the gc-content-percentages for each processed read

    Returns
    -------
    results : list of dicts
        Contains the input for the Vega-Lite-plot to be embedded as 'values'
    """
    result = []
    original_len = len(data)
    data = np.array(data) * 100
    data = data.astype(int)
    median = np.median(data)
    std = np.std(data)
    data = np.bincount(data)
    for _ in range(len(data), 101):
        data = np.append(data, [0])
    for (i, occurrences) in enumerate(data):
        result.append({"count": int(occurrences), "gc_pct": i, "type": "gc"})
    normal = np.random.normal(median, std, original_len)
    normal = np.around(normal).astype(int)
    normal = [number for number in normal if 0 <= number <= 100]
    normal_data = np.bincount(normal)
    for _ in range(len(normal_data), 101):
        normal_data = np.append(normal_data, [0])
    for (i, occurrences) in enumerate(normal_data):
        result.append(
            {"count": int(occurrences), "gc_pct": i, "type": "normal_distribution"}
        )
    return result


def process_quality_per_pos_data(data):
    """Processes distribution of phred scores per position in a read to be embedded into Vega-Lite-plot.

    Parameters
    ----------
    data : array of arrays of type np.uint32
        The outer array represents the position in a read.
        The inner array contains the occurrences of each phred score. (phred score is represented by the index)

    Returns
    -------
    results : list of dicts
        Contains the input for the Vega-Lite-plot to be embedded as 'values'
    """
    result = []
    for pos, qualities in enumerate(data, start=1):
        values = []
        count = 0
        for score, quantity in enumerate(qualities):
            if quantity > 0:
                values.append(score)
                count += quantity
        if not values:
            continue
        arangedQuals = np.arange(94)
        average = np.average(arangedQuals, weights=qualities)
        q1_border = count * 0.25
        q3_border = count * 0.75
        median_border = count * 0.5
        q1_found = q3_found = median_found = False
        for score, quantity in enumerate(qualities):
            if q1_border - quantity <= 0 and not q1_found:
                q1 = score
                q1_found = True
            else:
                q1_border = q1_border - quantity
            if q3_border - quantity <= 0 and not q3_found:
                q3 = score
                q3_found = True
            else:
                q3_border = q3_border - quantity
            if median_border - quantity <= 0 and not median_found:
                median_found = True
                median = score
            else:
                median_border = median_border - quantity
        iqr = q3 - q1
        loval = q1 - 1.5 * iqr
        hival = q3 + 1.5 * iqr
        upper = np.max([item for item in values if item <= hival])
        lower = np.min([item for item in values if item >= loval])
        entry = {
            "pos": int(pos),
            "upper": float(upper),
            "lower": float(lower),
            "median": float(median),
            "average": float(average),
            "q1": float(q1 - 0.1),
            "q3": float(q3 + 0.1),
            "outliers": [],
        }
        result.append(entry)
    return result
