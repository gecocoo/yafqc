"""YAFQC Argparser File
Generates a user-friendly command-line interface.

This file contains the following functions:

    * report - Creates an HTML report of a previously processed FASTQ file.
    * main - This is the main function for calling the tool. An argument parser is defined here.

Two subcommands are available:
    * process
    * report
    * contamination

This file requires argparse to be installed.
"""
import argparse
import os

from yafqc.report.report import create_report
from yafqc.process import main as main_process
from yafqc.contamination import main as main_contamination


def report(args):
    """Calls the create_report function with with arguments provided by the argparser."""
    create_report()


def get_parser():
    description = """Yet Another FastQ Quality Control

YAFQC is a quality control tool for analyzing DNA reads with regards to their quality.
It reads a FASTQ file which is then processed to calculate various statistics.

Available statistics are:
    * read length
    * sequence quality score
    * per base sequence quality
    * per base sequence content
    * per base N content
    * per base GC content
    * kmer content
    * contamination 


There are three subcommands: 
    * `process` to process a FASTQ file 
    * `report` to create a HTML report for previously calculated results (JSON format)
    * `contamination` to to process FASTA files and generate a hashtable for reference genomes
"""

    parser = argparse.ArgumentParser(
        description=description,
        epilog="by project group GeCoCoo, TU Dortmund University.",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    subparsers = parser.add_subparsers(
        help="Available subcommands. A subcommand is required."
    )

    ### SUBCOMMAND PROCESS

    process_help = """Processes given FASTQ files by calculating needed statistics.
    If reference hashtable is given, usage of same k-mer length for contamination check as in the reference table generation, but
    other statistics are calculated with given k.
    Results are written to stdout (JSON format) and can be visualized by the `report` subcommand.
    Example: 'yafqc process example.fastq > results.json'"""

    canonical_help = """flag for using original kmers instead of canonical kmers.
    Canonical kmers are the minimum of the original kmer and its reverse complement.
    By default canonical kmers are used."""

    parser_process = subparsers.add_parser(
        "process", help=process_help, formatter_class=argparse.RawTextHelpFormatter
    )
    parser_process.add_argument("fastq", metavar="FASTQ", help="FASTQ file to process.")
    parser_process.add_argument(
        "-r",
        "--reference",
        metavar="FILE",
        type=str,
        default=None,
        help="""the file with the hashtable containing the reference genomes, created  by the `contamination` subcommand""",
    )
    parser_process.add_argument(
        "-t",
        "--threadcount",
        metavar="INT",
        type=int,
        default=0,
        help="""maximum number of threads for processing the data.
    By default the number of logical cores is used.""",
    )
    parser_process.add_argument(
        "-k",
        "--kmer",
        metavar="INT",
        type=int,
        default=5,
        help="""length k of k-mers for kmer counting.
    By default this is 5.""",
    )
    parser_process.add_argument(
        "-c",
        "--canonical",
        action="store_false",
        help=canonical_help,
    )
    parser_process.add_argument(
        "--config",
        default="",
        help="config file for the gecocoo hashtable for kmer counting.",
    )
    parser_process.add_argument(
        "--nohash",
        action="store_true",
        help="development flag. disables kmer counting.",
    )
    parser_process.add_argument(
        "-s", "--size", type=int, help="""size of the hashtable."""
    )

    parser_process.set_defaults(func=main_process)

    ### SUBCOMMAND REPORT

    report_help = """Creates an html report from previously calculated statistics. The input is read from stdin and written to stdout. 
    Example: yafqc report < results.json > report.html
    """
    parser_report = subparsers.add_parser("report", help=report_help)
    parser_report.set_defaults(func=report)

    ### SUBCOMMAND CONTAMINATION

    contamination_help = """Processes given FASTA files by creating hashtable of the files.
        Stores the occurrence for all unique k-mers for each file."""

    parser_contamination = subparsers.add_parser(
        "contamination",
        help=contamination_help,
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser_contamination.add_argument(
        "fasta", metavar="FASTA", nargs="+", help="FASTA file to process."
    )
    parser_contamination.add_argument(
        "-t",
        "--threadcount",
        metavar="INT",
        type=int,
        default=0,
        help="""maximum number of threads for processing the data.
    By default the number of logical cores is used.""",
    )
    parser_contamination.add_argument(
        "-k",
        "--kmer",
        metavar="INT",
        type=int,
        default=5,
        help="""length k of k-mers for kmer counting.
    By default this is 5.""",
    )
    parser_contamination.add_argument(
        "-c",
        "--canonical",
        action="store_false",
        help=canonical_help,
    )
    parser_contamination.add_argument(
        "-o",
        "--output",
        default="reftable.gecocoo",
        help="""output file to save the hashtable.""",
    )
    parser_contamination.add_argument(
        "--config", default="", help="""config file for the gecocoo hashtable."""
    )
    parser_contamination.add_argument(
        "-s", "--size", type=int, help="""size of the hashtable."""
    )

    parser_contamination.set_defaults(func=main_contamination)
    return parser


def main():

    parser = get_parser()

    args = parser.parse_args()

    if hasattr(args, "func"):
        args.func(args)
    else:
        parser.print_help()


if __name__ == "__main__":
    main()
