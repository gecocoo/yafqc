""" FASTQ Processing File
Offers functionalty for generating a FASTQ processing function. 
This is possible on Single-Read and Paired-End FASTQ files, it offers multithreading and chunkwise reading and
processing of FASTQ files.

This file requires 'numpy' and 'numba' to be installed on the device it is going
to be used on.

This file can be imported by another .py file and contains the following
functions:

    * make_process_read_from_fastq - Creates a function for reading and processing FASTQ files. Users may decide, which output format of processing steps is needed.

"""

import numpy as np
from numba import njit
import datetime
import json
import os
import multiprocessing
import sys

from gecocoo.generator import generate_hashtable
from gecocoo.valuemodules import store, counting
from gecocoo.config import (
    Config,
    HashFunction,
    ValueModuleConfig,
    StorageConfig,
    StorageMode,
)
from gecocoo.config.file import load_from_yaml
from gecocoo.helper import get_qualified_name
from gecocoo.hashing import xorlinear, swaplinear
from gecocoo.table.serialize import deserialize
from gecocoo.config.lowlevel import ConfigError

from concurrent.futures import ThreadPoolExecutor, as_completed
from math import ceil
from strictyaml.exceptions import YAMLValidationError

from yafqc.kmer import process_kmers
from yafqc.kmer import number_canonicals
from yafqc.dnaio import fastq_chunks
from yafqc.dna_encode import quick_dna_to_2bits
from yafqc.utils import log

from numba.core.errors import NumbaExperimentalFeatureWarning
import warnings

from yafqc.report.report import (
    process_sequence_quality_scores,
    process_gc_content,
    process_distance_data,
)

warnings.simplefilter("ignore", category=NumbaExperimentalFeatureWarning)


@njit(nogil=True)
def get_border(linemarks, threads):
    """Finds border-indices for each chunk depending on the number of used threads

    Parameters
    ----------
    linemarks : 2-dimensional numpy array of type uint32
        Array containing the start- and end-indices of each Read.
        - linemarks[i][0] Start index of the i-th sequence
        - linemarks[i][1] End index of the i-th sequence
        - linemarks[i][2] Start index of the i-th Read
        - linemarks[i][3] End index of the i-th Read
        - e.g. [54, 155, 0, 260] for a sequence of the length 101
    threads : int
        Number of Threads, which determines in how many parts the chunk has to be cut.

    Returns
    -------
    borders : numpy array of type uint32
        Contains the start- and end-indices for each Thread. The end-index of Thread i is the start-index of Thread i+1.
        - e.g. [  0,  17,  34,  51,  68,  85, 102, 119, 136, 153, 170, 187, 200] for 12 Threads (13 indices)
        - The first thread processes the first 17 of 200 reads/sequences
    """
    borders = np.empty(threads + 1, dtype=np.uint32)
    for i in range(threads):
        borders[i] = min(ceil(linemarks.shape[0] / threads) * i, linemarks.shape[0])
    borders[threads] = linemarks.shape[0]
    return borders


def make_process_read_from_fastq(
    input_size,
    threads,
    cli_k,
    canonical,
    config,
    nohash=False,
    bufsize=2 ** 23,
    chunkreads=2 ** 23,
    reference=None,
    tablesize=None,
):
    """Generate a process function for FASTQ file reading.

    Parameters
    ----------
    input_size : int
        Size of input file in bytes
    threads : int
        Number of Threads
    k : int
        Length of each k-mer
    canonical : Bool
        If canonical k-mers should be used
    config : str
        Optional path to a config file for the hashtable
    nohash : Bool
        Development flag for deactivating all hashtable uses
    bufsize : int
        Size of created buffer, which will contain the Reads
    chunkreads : int
        Maximum number of Reads, which can be stored in the buffer
    reference : str
        Optional Path to a reference genome hashtable file
    Returns
    -------
    Generated function.
    """

    ref_k = 1
    ref_number = 1
    # names = []

    if reference:
        ref_table, utils = deserialize(reference)
        ref_k = utils.llconfig.k
        ref_number = utils.metadata["ref_number"]
        names = utils.metadata["files"]

        @njit
        @process_kmers(k=ref_k, use_canonical=canonical)
        @njit(nogil=True)
        def count_kmers_ref(kmer: int, uniques_pos):
            """Function to be called for each k-mer in a sequence to count the occurrence of each k-mer by the use of
            a decorator.

            Parameters
            ----------
            k : int
            Length of each k-mer.
            include_rev_compl : Bool
                Flag that indicates whether the reverse complement of each k-mer shall be also counted.
            kmer: int
                Encoded representation of the seen k-mer, which shall be counted.
            counter : numpy ndarray
                Array containing the number of each k-mer occurring at the index of the integer encoded k-mer.

            Returns
            -------
            """
            uniques = uniques_pos[0]
            ordered = uniques_pos[1]
            pos = uniques_pos[2]
            uniques.add(kmer)
            ordered[pos] = kmer
            pos[0] += 1

    if not nohash:

        if config:
            with open(config) as config_file:
                try:
                    hash_conf = load_from_yaml(config_file)
                    if threads > 1 and (
                        hash_conf.storage is None or not hash_conf.storage.concurrent
                    ):
                        log(
                            "Warning: Number of threads is > 1 but concurrent mode is set to False. Processing will take longer.",
                            file=sys.stderr,
                        )
                    if hash_conf.k != cli_k:
                        log(
                            f"""Warning: k given in config file ({hash_conf.k}) differs from k given in command line: {cli_k}
                        Prioritizing config file.""",
                            file=sys.stderr,
                        )
                    if hash_conf.value_module.module_name != get_qualified_name(
                        counting
                    ):
                        log(f"Error: Value module differs.")
                        sys.exit(1)
                except YAMLValidationError as err:
                    log("Error: failed loading config file: " + config)
                    log(err)
                    sys.exit(1)
        else:
            mode = False
            if threads > 1:
                mode = True
            hash_conf = Config(
                load_factor=0.95,
                bucket_size=4,
                k=cli_k,
                value_module=ValueModuleConfig(get_qualified_name(counting)),
                hash_functions=[
                    HashFunction(get_qualified_name(xorlinear), []),
                    HashFunction(get_qualified_name(xorlinear), []),
                ],
                storage=StorageConfig(
                    mode=StorageMode.BUCKETS,
                    concurrent=mode,
                    disable_quotienting=False,
                ),
            )

        # estimate number of k-mers in the file
        # input_size : number of bytes of input file
        if hash_conf.n:
            tablesize = hash_conf.n
        elif tablesize is None:
            seq_size = int(input_size)
            expected_kmers = max(0, seq_size - hash_conf.k + 1)
            if canonical:
                comb_kmers = number_canonicals(hash_conf.k)
            else:
                comb_kmers = 4 ** hash_conf.k

            tablesize = min(expected_kmers, comb_kmers)

        log("tablesize:", tablesize)

        try:
            hashtable, _ = generate_hashtable(hash_conf, tablesize)
        except ConfigError as err:
            log("Error: Unable to generate hashtable. Error message:")
            log(err)
            sys.exit(1)

        insert = hashtable.insert_single

        @njit
        @process_kmers(k=hash_conf.k, use_canonical=canonical)
        @njit(nogil=True)
        def count_kmers(kmer: int, storage):
            """Function to be called for each k-mer in a sequence to count the occurrence of each k-mer by the use of
            a decorator.

            Parameters
            ----------
            k : int
            Length of each k-mer.
            include_rev_compl : Bool
                Flag that indicates whether the reverse complement of each k-mer shall be also counted.
            kmer: int
                Encoded representation of the seen k-mer, which shall be counted.
            counter : numpy ndarray
                Array containing the number of each k-mer occurring at the index of the integer encoded k-mer.

            Returns
            -------
            """

            insert(storage, kmer)

    @njit(nogil=True)
    def processor(
        buf,
        linemarks,
        storage,
    ):
        """Function for processing of each chunk.

        Parameters
        ----------
        buf : numpy array
            Contains all characters of the first input file as byte.
        linemarks : 2-dimensional numpy array of type uint32
            Array containing the start- and end-indices of each read of the first input file.
            - linemarks[i][0] Start index of the i-th sequence
            - linemarks[i][1] End index of the i-th sequence
            - linemarks[i][2] Start index of the i-th Read
            - linemarks[i][3] End index of the i-th Read
            - e.g. [54, 155, 0, 260] for a sequence of the length 101

        Returns
        -------
        Generated hashtable containing the occurence of each possible k-mer in this chunk.
        """
        n = linemarks.shape[0]
        qualities = np.zeros(
            (0, 94), dtype=np.uint32
        )  # for quality counts per position
        read_lengths = {}
        mean_quals = np.zeros((n), dtype=np.double)
        characters = np.zeros((0, 5), dtype=np.uint32)
        gc_content = np.zeros((n), dtype=np.double)
        test = int(5)
        kmers = {test}
        kmers.clear()
        distances = np.zeros((n, ref_number), dtype=np.double)
        base_distances = np.zeros((n, ref_number), dtype=np.double)
        position = np.zeros(1, dtype=np.int64)

        # processing of first file
        for i in range(n):
            sq = buf[linemarks[i, 0] : linemarks[i, 1]]
            # count_kmers(sq, storage)
            kmers_base = np.zeros(len(sq) - ref_k, np.uint64)
            position[0] = 0
            # print(position[0])
            if reference:
                count_kmers_ref(sq, (kmers, kmers_base, position))
            if not nohash:
                count_kmers(sq, args=storage)
            quick_dna_to_2bits(sq)
            if len(sq) in read_lengths:
                read_lengths[len(sq)] += 1
            else:
                read_lengths[len(sq)] = 1
            qs = buf[linemarks[i, 4] : linemarks[i, 3] - 1]
            qualis = np.zeros((len(qs), 94), dtype=np.uint32)
            char_per_pos = np.zeros((len(sq), 5), dtype=np.uint32)
            qual_sum = 0
            for j, c in enumerate(qs):
                if 33 <= c <= 126:
                    qualis[j, c - 33] = 1
                    qual_sum += c - 33
            acgt = np.zeros(4, dtype=np.uint32)
            for j, c in enumerate(sq):
                if c < 4:
                    char_per_pos[j, c] = 1
                    acgt[c] += 1
            gc_sum = acgt[1] + acgt[2]
            acgt_sum = acgt[0] + acgt[1] + acgt[2] + acgt[3]
            if acgt_sum == 0:
                gc_content[i] = 0
            else:
                gc_content[i] = np.divide(gc_sum, acgt_sum)
            mean_quals[i] = np.divide(qual_sum, (len(sq)))
            if qualities.shape[0] == 0:
                qualities = np.zeros((len(qs), 94), dtype=np.uint32)

            if characters.shape[0] == 0:
                characters = np.zeros((len(sq), 5), dtype=np.uint32)

            # if necessary, increase size of quality array (create a bigger one and copy content)
            if qualis.shape[0] > qualities.shape[0]:
                qcopy = qualities
                qualities = np.zeros(qualis.shape, dtype=np.uint32)

                for k in range(qcopy.shape[0]):
                    for j in range(qcopy.shape[1]):
                        qualities[k, j] = qcopy[k, j]

            if char_per_pos.shape[0] > characters.shape[0]:
                qcopy = characters
                characters = np.zeros(char_per_pos.shape, dtype=np.uint32)

                for k in range(qcopy.shape[0]):
                    for j in range(qcopy.shape[1]):
                        characters[k, j] = qcopy[k, j]

            # add this read's quality score counter to the global counter
            for j in range(qualis.shape[0]):
                qualities[j] = np.add(qualities[j], qualis[j])

            for j in range(char_per_pos.shape[0]):
                characters[j] = np.add(characters[j], char_per_pos[j])
            if reference:
                kmer_counter = np.zeros(ref_number)
                base_counter = np.zeros(ref_number)
                last_pos = np.zeros(ref_number, dtype=np.int64)
                for pos, key in enumerate(kmers_base):
                    if pos < position[0]:
                        flag = ref_table.search_single(ref_table.storage, key)
                        for j in range(len(kmer_counter)):
                            if flag is not None and (flag & 2 ** j) > 0:
                                dif = ref_k + (pos - last_pos[j])
                                base_counter[j] += min(dif, ref_k)
                                last_pos[j] = pos + ref_k
                for key in kmers:
                    # print(key, pos)
                    flag = ref_table.search_single(ref_table.storage, key)
                    for j in range(len(kmer_counter)):
                        if flag is not None and (flag & 2 ** j) > 0:
                            kmer_counter[j] += 1
                if kmers:
                    dist = np.divide(kmer_counter, len(kmers))
                else:
                    dist = np.zeros(ref_number)
                base_dist = np.divide(base_counter, position[0] + ref_k - 1)
                distances[i] = dist
                base_distances[i] = base_dist
                kmers.clear()

        return (
            qualities,
            read_lengths,
            mean_quals,
            characters,
            gc_content,
            distances,
            base_distances,
        )

    def process_read_from_fastq(fastq):
        """Process function for FASTQ file reading.

        Parameters
        ----------
        fastq : file
            First input file which is given as Commandline Argument

        Returns
        -------
        Generated hashtable containing the occurence of each possible k-mer.
        """
        with ThreadPoolExecutor(max_workers=threads) as executor:

            """Reading input files
            depending on the number of input files (1 or 2 files) a reading method is called
            as many chunks as needed to store the input files are created and processed afterwards
            a chunk is a tuple of two arrays
            - the first contains all characters of the file as byte
            - the second is an array with 4 indices:
                - [start of sequence, end of sequence, start of whole entry, end of whole entry]
                - e.g. [ 54, 155, 0, 260] for the first entry of our FASTQ-file with a sequence-length of 101
            """

            """ Initialization """
            characters = np.zeros((0, 5), dtype=np.uint32)
            read_lengths = {}
            qualities = np.zeros((0, 94), dtype=np.uint32)
            mean_read_qualities = np.zeros(0, dtype=np.uint32)
            gc_content = np.zeros(0, dtype=np.double)
            distances = np.zeros((0, ref_number), dtype=np.double)
            base_distances = np.zeros((0, ref_number), dtype=np.double)
            for chunk in fastq_chunks(
                fastq, bufsize=bufsize * threads, maxreads=chunkreads * threads
            ):
                borders = get_border(chunk[1], threads)
                if nohash:
                    futures = [
                        executor.submit(
                            processor,
                            chunk[0],
                            chunk[1][borders[i] : borders[i + 1]],
                            None,
                        )
                        for i in range(threads)
                    ]
                else:
                    futures = [
                        executor.submit(
                            processor,
                            chunk[0],
                            chunk[1][borders[i] : borders[i + 1]],
                            hashtable.storage,
                        )
                        for i in range(threads)
                    ]
                """ Handling of each result """
                for i in as_completed(futures):
                    (q, rl, mq, c, gc, d, bd) = i.result()
                    if qualities.shape[0] == 0:
                        qualities = np.zeros(q.shape, dtype=np.uint32)
                    if characters.shape[0] == 0:
                        characters = np.zeros(c.shape, dtype=np.uint32)
                    if q.shape[0] > qualities.shape[0]:
                        qcopy = qualities
                        qualities = np.zeros(q.shape, dtype=np.uint32)
                        for i in range(qcopy.shape[0]):
                            for j in range(qcopy.shape[1]):
                                qualities[i, j] = qcopy[i, j]
                    if c.shape[0] > characters.shape[0]:
                        qcopy = characters
                        characters = np.zeros(c.shape, dtype=np.uint32)
                        for i in range(qcopy.shape[0]):
                            for j in range(qcopy.shape[1]):
                                characters[i, j] = qcopy[i, j]
                    for i in range(q.shape[0]):
                        qualities[i] = np.add(qualities[i], q[i])
                    for i in range(c.shape[0]):
                        characters[i] = np.add(characters[i], c[i])
                    for key in rl.keys():
                        if key in read_lengths:
                            read_lengths[key] += rl[key]
                        else:
                            read_lengths[key] = rl[key]
                    mean_read_qualities = np.concatenate((mean_read_qualities, mq))
                    gc_content = np.concatenate((gc_content, gc))
                    if reference:
                        distances = np.concatenate((distances, d))
                        base_distances = np.concatenate((base_distances, bd))

            counter = {}
            if not nohash:
                for key, value in hashtable.key_value_iterator(hashtable.storage):
                    counter[key] = value

            """ Retrieve actual file name from file path """
            if reference:
                split_names = list(map(lambda name: os.path.split(name)[1], names))
            """ Preprocess output data to reduce output file size """
            mean_read_qualities = process_sequence_quality_scores(
                mean_read_qualities.tolist()
            )
            gc_content = process_gc_content(gc_content.tolist())
            if reference:
                distances = process_distance_data(distances.tolist(), split_names)
                base_distances = process_distance_data(
                    base_distances.tolist(), split_names
                )
            """ Dump results into JSON file """
            if reference:
                output = {
                    "file_name": fastq,
                    "threads": threads,
                    "canonical": canonical,
                    "k": hash_conf.k,
                    "counter": counter,
                    "quality_per_pos": qualities.tolist(),
                    "read_lengths": read_lengths,
                    "mean_read_qualities": mean_read_qualities,
                    "character_per_pos": characters.tolist(),
                    "gc_content": gc_content,
                    "distances": distances,
                    "base_distances": base_distances,
                    "names": names,
                    "ref_k": ref_k,
                }
            else:
                output = {
                    "file_name": fastq,
                    "threads": threads,
                    "canonical": canonical,
                    "k": cli_k if nohash else hash_conf.k,
                    "counter": counter,
                    "quality_per_pos": qualities.tolist(),
                    "read_lengths": read_lengths,
                    "mean_read_qualities": mean_read_qualities,
                    "character_per_pos": characters.tolist(),
                    "gc_content": gc_content,
                }

            sys.stdout.reconfigure(encoding="utf-8")
            json.dump(
                output,
                sys.stdout,
                separators=(",", ":"),
                sort_keys=True,
                indent=4,
            )

    return process_read_from_fastq


def main(args):
    """ main method for quality control reads"""

    if not os.path.isfile(args.fastq):
        log("FASTQ file not found: " + args.fastq)
        sys.exit(1)
    if args.config and not os.path.isfile(args.config):
        log("config file not found: " + args.config)
        sys.exit(1)

    now = datetime.datetime.now()
    log(f"#{now:%Y-%m-%d %H:%M:%S}: Begin processing")
    if args.fastq:
        if args.threadcount == 0:
            threads = multiprocessing.cpu_count()
        else:
            threads = args.threadcount
        process_read_from_fastq = make_process_read_from_fastq(
            os.stat(args.fastq).st_size,
            threads,
            args.kmer,
            args.canonical,
            args.config,
            reference=args.reference,
            nohash=args.nohash,
            tablesize=args.size,
        )
        process_read_from_fastq(args.fastq)
        now = datetime.datetime.now()
        log(f"#{now:%Y-%m-%d %H:%M:%S}: All Reads processed")
    else:
        assert False
