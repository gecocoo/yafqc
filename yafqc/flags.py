from gecocoo.config import Config, HashFunction, ValueModuleConfig
from gecocoo.generator import generate_hashtable
from gecocoo.helper import get_qualified_name
from gecocoo.valuemodules import ValueModuleBase
from gecocoo.hashing import xorlinear
from numba import byte, njit, uint64
from numba.types import Type, UniTuple


def flags_module(bits=32):
    @njit(uint64(uint64, uint64))
    def first_insert(key, flag):
        return flag

    @njit(uint64(uint64, uint64, uint64))
    def update_value(key, stored_value, flag):
        return stored_value | flag

    @njit(uint64(uint64, uint64))
    def map_value(key, stored_value):
        return stored_value

    return (first_insert, update_value, map_value, bits)


def generate_flags(n):
    flags = []
    for i in range(n):
        flags.append(2 ** (i))

    return flags


# if __name__ == "__main__":

#     # for experiments and tests

#     print("generate flags")
#     no_flags = 10
#     flags = generate_flags(no_flags)
#     # bin_flags = [bin(f) for f in flags]
#     # print(flags)
#     # print(bin_flags)

#     print("generate hashtable")
#     config = Config(
#         load_factor=0.95,
#         k=32,
#         bucket_size=4,
#         value_module=ValueModuleConfig(module_name=get_qualified_name(flags_module)),
#         hash_functions=[
#             HashFunction(get_qualified_name(xorlinear)),
#             HashFunction(get_qualified_name(xorlinear)),
#             HashFunction(get_qualified_name(xorlinear)),
#         ],
#     )

#     (table, _) = generate_hashtable(config, 10 ** 3)

#     # table.insert_single(table.storage, 3, flags[0])
#     # table.insert_single(table.storage, 3, flags[2])
#     # table.insert_single(table.storage, 3, flags[7])
#     # r = table.search_single(table.storage, 3)
#     # print(bin(r))

#     print("inserts")
#     table.insert_single(table.storage, 3, flags[0])
#     table.insert_single(table.storage, 3, flags[1])
#     table.insert_single(table.storage, 3, flags[0])
#     table.insert_single(table.storage, 3, flags[1])
#     r = table.search_single(table.storage, 3)
#     print(bin(r))
