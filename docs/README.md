# Installation
Using pip
```
pip install sphinx sphinx-rtd-theme
```
for other Package-Manager/Operating-Systems have a look at: https://www.sphinx-doc.org/en/master/usage/installation.html

# Usage
#### Script
```
sh docs/build.sh
```

#### Manual 
from 'docs' directory
```
sphinx-apidoc -f -o source/hashtable ../gecocoo/gecocoo
sphinx-apidoc -f -o source/yafqc ../yafqc

make html
```

[comment]: <> (**How to generate/work with the Doc:**)

[comment]: <> ([comment]: <> &#40;* 1. `pip install -U Sphinx astropy-sphinx-theme` - Installs Sphinx and the theme&#41;)

[comment]: <> ([comment]: <> &#40;* 2. `make html` - generates HTML from .rst in source.&#41;)

[comment]: <> (**For debugging purpose**)

[comment]: <> (* 1. `sphinx-apidoc -o source ../lcfilter` - optional, generate .rst files from the package. Further documentations can added to these and moved to source.)

[comment]: <> (* 2. `make clean` - clears up artifacts.)
