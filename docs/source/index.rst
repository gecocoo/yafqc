Welcome to Yet Another FastQ Quality Control's documentation!
=============================================================
.. image:: _static/assets/gitlab_logo.svg
   :width: 100
   :alt: Repository
   :target: https://gitlab.com/gecocoo/yafqc

YAFQC is a quality control tool for analyzing DNA reads with regards to their quality. It can read a single FASTQ file or two paired FASTQ files which are then processed to calculate various statistics. It offers multithreading and chunkwise reading and processing of FASTQ files.

Available statistics are:
    * read length
    * sequence quality score
    * per base sequence quality
    * per base sequence content
    * per base N content
    * per base GC content
    * kmer content
    * contamination

The results can be summarized in a report that visualizes them.

---------------
Getting started
---------------
For an easy introduction to the tool, please have a look at the information in the :ref:`getting started guide<installation>`.
It describes the installation, the handling via CLI as well as the configuration.

---------------
API Reference
---------------
A complete documentation of the source code can be found in the api reference section.

.. toctree::
   :caption: Getting started
   :name: getting_started
   :hidden:
   :maxdepth: 1

   getting_started/installation
   getting_started/usage
   getting_started/configuration

.. toctree::
   :maxdepth: 2
   :caption: API Reference
   :name: api_reference
   :hidden:

   hashtable/gecocoo
   yafqc/yafqc

.. toctree::
   :maxdepth: 2
   :caption: Project Info
   :name: project_info
   :hidden:

   project_info/license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
