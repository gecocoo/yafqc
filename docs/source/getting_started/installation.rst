.. _installation:

Installation
========================
* Install Anaconda or miniconda
* `conda env create -f environment.yml`

Or update your existing environment

* `conda env update -f environment.yml`

When developing make sure to install the git hook scripts

* `pre-commit install`

**To start the tool:**

* `conda activate yafqc`
* `python setup.py develop`
* `yafqc process -q my_file.fastq`



