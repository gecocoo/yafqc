Usage
========================
.. argparse::
   :module: yafqc
   :func: get_parser
   :prog: yafqc

Examples
***********
Usage for *four threads*, *5-mers*, the config file *hashtable_config.yml* and fastq file *genome.fastq*, which are in the same directory.

* `yafqc process -t 4 -k 7 --config hashtable_config.yml genome.fastq`

To create a report from a previously created `results.json`:

* `yafqc report < results.json > report.html`

or directly use the output given from `yafqc process`:

* `yafqc process my_file.fastq | yafqc report > report.html`

Check out an example of a generated report `here <https://gecocoo.gitlab.io/yafqc/doc_report.html>`_.