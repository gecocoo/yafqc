Hashtable Configuration
========================
There are two options for configuring the hash table.
Either you generate the hashtable programmatically or you specify a configuration
file during execution via cli.

A detailed description of the configuration parameter and the usage can be found in the `hashtable documentation <https://gecocoo.gitlab.io/gecocoo/configuration/options.html>`_.
